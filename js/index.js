//modal menu

let modalIcon = document.querySelector('.home-menu');
let modal = document.querySelector('.modal-header-menu');
let closeModal = document.querySelector('.modal-menu-close');
let chooseMenu = document.querySelector('.modal-menu-hover');

modalIcon.addEventListener('click', () => {
    modal.classList.add('modal-show');
});

closeModal.addEventListener('click', hideModalMenu);
chooseMenu.addEventListener('click', hideModalMenu);

function hideModalMenu () {
  modal.classList.remove('modal-show');
}

//tabs

document.getElementById("defaultOpen").click();

function openCity(evt, cityName) {
    let tabContent, tabLinks;
  
    tabContent = document.getElementsByClassName("tabcontent");
    for (let i = 0; i < tabContent.length; i++) {
      tabContent[i].style.display = "none";
    }
  
    tabLinks = document.getElementsByClassName("tablinks");
    for (let i = 0; i < tabLinks.length; i++) {
      tabLinks[i].className = tabLinks[i].className.replace(" active", "");
    }
  
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }

//validation
let form = document.querySelector('.messaging-form');
let validateBtn = document.querySelector('.validateBtn');
let messagingName = form.querySelector('.messaging-name');
let messagingEmail = form.querySelector('.messaging-email');

form.addEventListener('submit', function (e) {
  e.preventDefault();
  console.log('messaging-name: ', messagingName.value);
  console.log('messaging-email: ', messagingEmail.value);
});

function checkUserName(str) {
  let letters =  /^[a-zA-Zа-яА-ЯёЁ'][a-zA-Z-а-яА-ЯёЁ' ]+[a-zA-Zа-яА-ЯёЁ']?$/;
  if(str.value.match(letters) && messagingName.value.length >= 5) {
      console.log('Right user name');
      str.style.borderColor = 'black';
    } else {
      console.log('Wrong user name');
      str.style.borderColor = 'red';
}};

function checkEmail(str) {
      if (str.value.length <= 5 
        && str.value.length < 256 
        || str.value.indexOf('@') < -1 
      || str.value.indexOf('@') == 0 
      || str.value.indexOf('@') == str.value.length -1) {
        console.log('Wrong email name');
        str.style.borderColor = 'red';        
      } else {
        console.log('Right email name');
        str.style.borderColor = 'black';
      }
};


form.addEventListener('submit', function (e) {
    checkUserName(messagingName);
    checkEmail(messagingEmail);
  });


//slider
let buttonNext = document.querySelectorAll('.btn-next');
let buttonBack = document.querySelectorAll('.btn-back');

function goToNext() {
  let list = document.querySelector('.list');
  let margin = parseInt(list.style.marginLeft);
  if(margin > -198) {
      let newMargin = margin - 99;
      list.style.marginLeft = newMargin + 'vw'; 
  }
}

for(let i = 0; i < buttonNext.length; i++) {
  buttonNext[i].addEventListener("click", goToNext);
}

function goToBack() {
  let list = document.querySelector('.list');
  let margin = parseInt(list.style.marginLeft);
  if(margin < 0) {
      let newMargin = margin + 99;
      list.style.marginLeft = newMargin + 'vw'; 
  }
}

for(let i = 0; i < buttonBack.length; i++) {
  buttonBack[i].addEventListener("click", goToBack);
}

//filter

let allWorksLink = document.querySelector('.all-works-link');
let brandingLink = document.querySelector('.branding-link');
let designLink = document.querySelector('.design-link');
let photographyLink = document.querySelector('.photography-link');

allWorksLink.addEventListener('click', () => {
  filterImg('.all-works');
});

brandingLink.addEventListener('click', () => {
  filterImg('.branding');
});

designLink.addEventListener('click', () => {
  filterImg('.design');
});

photographyLink.addEventListener('click', () => {
  filterImg('.photography');
});

function filterImg(className) {
  let allImg = document.querySelectorAll('.all-works');
  for (let i = 0; i < allImg.length; i++) {
    allImg[i].classList.remove('hide-portfolio-img');
  }
  let hideImg = document.querySelectorAll('.all-works:not('+className+')');
  for (let i = 0; i < hideImg.length; i++) {
    hideImg[i].classList.add('hide-portfolio-img');
    
  }

  let position = document.querySelector('.portfolio-gallery');
  position.style.justifyContent = 'center';
}


//map
let modalMap = document.getElementById('myModalMap');
let btnMap = document.getElementById("myBtnMap");
let closeMap = document.getElementsByClassName("close-map")[0];

btnMap.onclick = function() {
  modalMap.style.display = "block";
}

closeMap.onclick = function() {
  modalMap.style.display = "none";
}

